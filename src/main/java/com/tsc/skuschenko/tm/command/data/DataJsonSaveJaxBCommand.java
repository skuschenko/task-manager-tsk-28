package com.tsc.skuschenko.tm.command.data;

import com.tsc.skuschenko.tm.dto.Domain;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.EmptyXmlPathException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.Optional;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String APPLICATION_TYPE = "application/json";

    @NotNull
    private final String CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    private final String CONTEXT_FACTORY_JAXB =
            "org.eclipse.persistence.jaxb.JAXBContextFactory";
    @NotNull
    private final String DESCRIPTION = "save data in json file";
    @NotNull
    private final String MEDIA_TYPE = "eclipselink.media-type";
    @NotNull
    private final String NAME = "data-save-json-jaxb";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.setProperty(CONTEXT_FACTORY, CONTEXT_FACTORY_JAXB);
        showOperationInfo(NAME);
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileJsonPath("jaxb");
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyXmlPathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull JAXBContext jaxbContext =
                JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller jaxbMarshaller =
                jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.setProperty(MEDIA_TYPE, APPLICATION_TYPE);
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
