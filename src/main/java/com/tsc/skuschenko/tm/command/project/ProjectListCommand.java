package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.enumerated.Sort;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "show all projects";

    @NotNull
    private final String NAME = "project-list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("sort");
        System.out.println(Arrays.toString(Sort.values()));
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Project> projects = new ArrayList<>();
        @NotNull final IProjectService projectService
                = serviceLocator.getProjectService();
        if (sort.isEmpty()) {
            projects = projectService.findAll(userId);
        } else {
            @NotNull final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            projects = projectService.findAll(userId, sortType.getComparator());
        }
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
