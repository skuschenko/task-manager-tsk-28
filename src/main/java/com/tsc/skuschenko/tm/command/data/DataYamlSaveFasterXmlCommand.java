package com.tsc.skuschenko.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.tsc.skuschenko.tm.dto.Domain;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.exception.empty.EmptyJsonPathException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.Optional;

public final class DataYamlSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "save data in yaml file";

    @NotNull
    private final String NAME = "data-save-yaml";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        showOperationInfo(NAME);
        @NotNull final Domain domain = getDomain();
        @Nullable final String filePath =
                serviceLocator.getPropertyService().getFileYamlPath();
        Optional.ofNullable(filePath)
                .orElseThrow(EmptyJsonPathException::new);
        @NotNull final File file = new File(filePath);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream =
                new FileOutputStream(file);
        @NotNull final ObjectMapper objectMapper =
                new ObjectMapper(new YAMLFactory());
        @NotNull final String json =
                objectMapper.writerWithDefaultPrettyPrinter()
                        .writeValueAsString(domain);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
