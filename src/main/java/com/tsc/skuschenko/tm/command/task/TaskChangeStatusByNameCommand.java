package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "change task by name";

    @NotNull
    private final String NAME = "task-change-status-by-name";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("name");
        @NotNull final String value = TerminalUtil.nextLine();
        @NotNull final ITaskService taskService =
                serviceLocator.getTaskService();
        @Nullable Task task = taskService.findOneByName(userId, value);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task = taskService.changeStatusByName(
                userId, value, readTaskStatus()
        );
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
