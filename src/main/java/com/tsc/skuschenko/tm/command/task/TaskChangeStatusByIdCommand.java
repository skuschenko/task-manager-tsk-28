package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "change task by id";

    @NotNull
    private final String NAME = "task-change-status-by-id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final String userId =
                serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("id");
        @NotNull final String valueId = TerminalUtil.nextLine();
        @NotNull final ITaskService taskService =
                serviceLocator.getTaskService();
        @Nullable Task task = taskService.findOneById(userId, valueId);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        task = taskService.changeStatusById(
                userId, valueId, readTaskStatus()
        );
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
