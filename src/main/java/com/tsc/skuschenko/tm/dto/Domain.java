package com.tsc.skuschenko.tm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.model.User;
import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@JsonRootName("domain")
@XmlAccessorType(XmlAccessType.FIELD)
public class Domain implements Serializable {

    @JsonProperty("project")
    @XmlElement(name = "project")
    @JacksonXmlElementWrapper(localName = "project")
    @XmlElementWrapper(name = "projects")
    private List<Project> projects;

    @JsonProperty("task")
    @XmlElement(name = "task")
    @JacksonXmlElementWrapper(localName = "task")
    @XmlElementWrapper(name = "tasks")
    private List<Task> tasks;

    @JsonProperty("user")
    @XmlElement(name = "user")
    @JacksonXmlElementWrapper(localName = "user")
    @XmlElementWrapper(name = "users")
    private List<User> users;

}
